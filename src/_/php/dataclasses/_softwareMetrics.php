<?php
echo "<table id='softMetricsTable' class='table table-striped table-hover' >
			<thead>
				<tr>
					<th><a href='#' data-toggle='tooltip' title='Version (Git Tag)'>Version</a></th>
					<th><a href='#' data-toggle='tooltip' title='Lines of Code'>LOC</a></th>
					<th><a href='#' data-toggle='tooltip' title='Number of Methods'>NOM</a></th>
					<th><a href='#' data-toggle='tooltip' title='Number of Fields'>NOF</a></th>
					<th><a href='#' data-toggle='tooltip' title='Coupling Between Objects'>CBO</a></th>
					<th><a href='#' data-toggle='tooltip' title='Lack of Cohesion in methods 2'>LCOM2</a></th>
					<th><a href='#' data-toggle='tooltip' title='Weighter Method Complexity'>WMC</a></th>
					<th><a href='#' data-toggle='tooltip' title='Weight of Class'>WOC</a></th>
					<th><a href='#' data-toggle='tooltip' title='Tight Class Cohesion'>TCC</a></th>
					<th><a href='#' data-toggle='tooltip' title='Access to Foreign Data'>ATFD</a></th>
					<th><a href='#' data-toggle='tooltip' title='Number of Public Attributes'>NOPA</a></th>
					<th><a href='#' data-toggle='tooltip' title='Number of Accessor Methods'>NOAM</a></th>

				</tr>
			</thead>
		<tbody>";
	foreach($_SESSION["versions_array"] as $key => $value) {	
		  {
		  echo "<tr>";
		  echo "<td>" . $_SESSION["versions_array"][$key] . "</td>";
		  echo "<td>" . $_SESSION["loc"][$key] . "</td>";
		  echo "<td>" . $_SESSION["nom"][$key] . "</td>";
		  echo "<td>" . $_SESSION["nof"][$key] . "</td>";
		  echo "<td>" . round($_SESSION["cbo"][$key],3) . "</td>";
		  echo "<td>" . round($_SESSION["lcom"][$key],3) . "</td>";
		  echo "<td>" . round($_SESSION["wmc"][$key],3) . "</td>";
		  echo "<td>" . round($_SESSION["woc"][$key],3) . "</td>"; 
		  echo "<td>" . round($_SESSION["tcc"][$key],3) . "</td>";
			  echo "<td>" . round($_SESSION["atfd"][$key],3) . "</td>";
		  echo "<td>" . $_SESSION["nopa"][$key] . "</td>";
		  echo "<td>" . $_SESSION["noam"][$key] . "</td>";

		  echo "</tr>";
		  }
	}	  
echo "</tbody>
	  </table>";
	
?>

